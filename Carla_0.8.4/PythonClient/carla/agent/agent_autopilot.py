
from carla.agent.agent import Agent
from carla.client import VehicleControl


class ForwardAutopilot(Agent):
    """
    Simple derivation of Agent Class,
    A trivial agent agent that goes straight
    """
    def run_step(self, measurements, sensor_data, directions, target):
       control = VehicleControl()
       control = measurements.player_measurements.autopilot_control
       return control

        
